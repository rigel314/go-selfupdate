package main

import (
	"bufio"
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	flag.Parse()

	var key *rsa.PrivateKey

	// Generate a new key if one doesn't exist.
	f, err := os.Open("key.json")
	if err != nil {
		key, err = rsa.GenerateKey(rand.Reader, 4096)
		if err != nil {
			log.Fatal(err)
		}
		f, err := os.Create("key.json")
		if err != nil {
			log.Fatal(err)
		}
		enc := json.NewEncoder(f)
		enc.SetIndent("", "\t")
		enc.Encode(key)
		f.Close()
	} else {
		rd := json.NewDecoder(f)
		err := rd.Decode(&key)
		if err != nil {
			log.Fatal(err)
		}
		f.Close()
	}

	fileToSign := flag.Arg(0)
	f, err = os.Open(fileToSign)
	if err != nil {
		log.Fatal(err)
	}

	s512 := sha512.New()

	rd := bufio.NewReader(f)
	rd.WriteTo(s512)

	calcsum := make([]byte, 0, s512.Size())
	calcsum = s512.Sum(calcsum)

	sig, err := rsa.SignPSS(rand.Reader, key, crypto.SHA512, calcsum, nil)
	fmt.Println(hex.EncodeToString(sig))
}
