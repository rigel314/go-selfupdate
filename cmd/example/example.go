package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/rigel314/go-selfupdate/update"
)

var GIT_VER = "Uncontrolled"

func main() {
	var u bool
	flag.BoolVar(&u, "update", false, "pass '-update' when perfoming an update")

	flag.Parse()

	ud := update.NewDefaultUpdater("https://gitlab.com/rigel314/go-selfupdate/raw/master/cmd/example/go-selfupdate.json", GIT_VER, nil)

	if u {
		err := update.CompleteUpdateProcess(ud)
		if err != nil {
			log.Fatal(err)
			return
		}
		return
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, `<h1>`+GIT_VER+`</h1><br /><input type="button" value="update" onclick="window.location='/update';" />`)
	})

	http.HandleFunc("/update", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(update.StartUpdateProcess(ud))
	})

	fmt.Println("running")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
