package main

import (
	"crypto/rsa"
	"encoding/json"
	"log"
	"os"
	"text/template"
)

func main() {
	var key *rsa.PrivateKey

	keyf, err := os.Open("key.json")
	if err != nil {
		log.Fatal(err)
	}
	rd := json.NewDecoder(keyf)
	err = rd.Decode(&key)
	if err != nil {
		log.Fatal(err)
	}
	keyf.Close()

	tmpl, err := template.New("verify").Parse(tl)
	if err != nil {
		log.Fatal(err)
	}
	vf, err := os.Create("verify.go")
	if err != nil {
		log.Fatal(err)
	}
	defer vf.Close()

	tmpl.Execute(vf, key)
}

var tl = `package main

import (
	"crypto/rsa"
	"math/big"
)

func init() {
	pk.N.UnmarshalText([]byte("{{.N}}"))
}

var pk *rsa.PublicKey = &rsa.PublicKey {
	N: new(big.Int),
	E: {{.E}},
}
`
