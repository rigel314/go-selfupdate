go-selfupdate
======

# About
A golang library to allow a binary to self update.  I'm aiming for painless usage.

# Special Tags
This repo has a few special tags that are changed during development to test the self updater.
The following tags should not be relied upon:
* example-1.1.0
* example-1.2.0
* sign-1.1.0
* sign-1.2.0
