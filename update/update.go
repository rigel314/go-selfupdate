package update

import (
	"bufio"
	"bytes"
	"crypto"
	"crypto/rsa"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

var _ = fmt.Printf

var ErrNoAcceptableVersions = errors.New("No acceptable versions found")
var ErrUnverified = errors.New("Chceksum didn't match")
var ErrUnauthenticated = errors.New("Signature didn't verify")
var ErrNoErrorButFalse = errors.New("Implementation didn't provide an error")
var ErrCallOrder = errors.New("A function was called when it shouldn't have been")
var ErrTimeout = errors.New("A function took longer than expected")

func NewDefaultUpdater(latestJSONurl, actualVersion string, pubkey *rsa.PublicKey) Updater {
	exepath, err := os.Executable()
	if err != nil {
		return nil
	}

	exedir, exename := filepath.Split(exepath)

	var tmpname string
	var realname string

	if strings.HasPrefix(exename, ".tmp") {
		tmpname = exename
		realname = exename[4:]
	} else {
		tmpname = ".tmp" + exename
		realname = exename
	}

	return &defaultUpdater{
		latestJSONurl: latestJSONurl,
		actualVersion: actualVersion,
		realPath:      filepath.Join(exedir, realname),
		updatePath:    filepath.Join(exedir, tmpname),
		pubKey:        pubkey,
	}
}

func StartUpdateProcess(u Updater) error {
	b, err := u.FetchMetaData()
	if err != nil {
		return err
	}
	if !b {
		return ErrNoErrorButFalse
	}

	err = u.FetchExecutable()
	if err != nil {
		return err
	}

	b, err = u.VerifyExecutable()
	if err != nil {
		return err
	}
	if !b {
		return ErrUnverified
	}

	err = u.LaunchForUpdate()
	if err != nil {
		return err
	}

	u.Exit()

	return nil
}

func CompleteUpdateProcess(u Updater) error {
	err := u.DoUpdate()
	if err != nil {
		return err
	}

	err = u.NormalLaunch()
	if err != nil {
		return err
	}

	u.ExitUpdate()

	return nil
}

type Updater interface {
	FetchMetaData() (shouldUpdate bool, e error)
	FetchExecutable() error
	VerifyExecutable() (good bool, e error)
	LaunchForUpdate() error
	Exit()
	DoUpdate() error
	NormalLaunch() error
	ExitUpdate()
}

var _ Updater = new(defaultUpdater)

type defaultUpdater struct {
	latestJSONurl string
	actualVersion string
	realPath      string
	updatePath    string
	pubKey        *rsa.PublicKey
	p             *platformURL
}

func (du *defaultUpdater) FetchMetaData() (shouldUpdate bool, e error) {
	resp, err := http.Get(du.latestJSONurl)
	if err != nil {
		return false, err
	}

	dec := json.NewDecoder(resp.Body)
	md := defaultJSONformat{}
	err = dec.Decode(&md)
	if err != nil {
		return false, err
	}
	err = resp.Body.Close()
	if err != nil {
		return false, err
	}

	thisPlatform := runtime.GOOS + "-" + runtime.GOARCH
	acceptableVersions := make([]versionJSON, 0, len(md))

	for _, v := range md {
		if _, platOk := v.Platforms[thisPlatform]; platOk {
			for _, allowed := range v.AllowedToUpdate {
				if allowed == du.actualVersion {
					acceptableVersions = append(acceptableVersions, v)
					break
				}
			}
		}
	}

	if len(acceptableVersions) == 0 {
		return false, ErrNoAcceptableVersions
	}

	if acceptableVersions[0].Version == du.actualVersion {
		return false, nil // Latest version matches actualVersion
	}

	p := acceptableVersions[0].Platforms[thisPlatform]
	du.p = &p

	return true, nil
}

func (du *defaultUpdater) FetchExecutable() error {
	if du.p == nil {
		return ErrCallOrder
	}

	f, err := os.OpenFile(du.updatePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		return err
	}
	defer f.Close()

	resp, err := http.Get(du.p.ExecutableURL)
	if err != nil {
		return err
	}

	rd := bufio.NewReader(resp.Body)
	_, err = rd.WriteTo(f)
	if err != nil {
		return err
	}
	err = resp.Body.Close()
	if err != nil {
		return err
	}

	return err
}

func (du *defaultUpdater) VerifyExecutable() (good bool, e error) {
	if du.p == nil {
		return false, ErrCallOrder
	}

	s512 := sha512.New()

	f, err := os.Open(du.updatePath)
	if err != nil {
		return false, err
	}
	defer f.Close()

	rd := bufio.NewReader(f)
	rd.WriteTo(s512)

	calcsum := make([]byte, 0, s512.Size())
	calcsum = s512.Sum(calcsum)

	websum, err := hex.DecodeString(du.p.SHA512)
	if err != nil {
		return false, err
	}

	if bytes.Compare(calcsum, websum) != 0 {
		return false, nil
	}

	if du.pubKey == nil {
		return true, nil
	}

	sig, err := hex.DecodeString(du.p.Signature)
	if err != nil {
		return false, err
	}
	err = rsa.VerifyPSS(du.pubKey, crypto.SHA512, calcsum, sig, nil)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (du *defaultUpdater) LaunchForUpdate() error {
	cmd := exec.Command(du.updatePath, "-update")
	return cmd.Start()
}

func (du *defaultUpdater) Exit() { os.Exit(0) }

func (du *defaultUpdater) DoUpdate() error {
	for i := 0; i < 11; i++ {
		if i == 10 {
			return ErrTimeout
		}

		time.Sleep(time.Second)

		fout, err := os.Create(du.realPath)
		if err != nil {
			continue
		}
		defer fout.Close()

		fin, err := os.Open(du.updatePath)
		if err != nil {
			continue
		}
		defer fin.Close()

		rd := bufio.NewReader(fin)
		_, err = rd.WriteTo(fout)
		if err != nil {
			return err
		}
		break
	}

	return nil
}

func (du *defaultUpdater) NormalLaunch() error {
	cmd := exec.Command(du.realPath)
	return cmd.Start()
}

func (du *defaultUpdater) ExitUpdate() { du.Exit() }

type defaultJSONformat []versionJSON

type versionJSON struct {
	Version         string
	Platforms       map[string]platformURL
	AllowedToUpdate []string `json:"Allowed To Update"`
}

type platformURL struct {
	ExecutableURL string
	SHA512        string
	Signature     string
}
