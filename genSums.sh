#!/bin/bash

VER="$1"

function sum() {
    cd /builds/rigel314/go-selfupdate
    FILEPATH="$1"
    VERinner="$(basename $FILEPATH)-$2"
    OS="$3"
    ARCH="$4"
    FILE="$5"
    EXT="$6"

    echo "$FILEPATH: $OS-$ARCH"
    GOOS="$OS" GOACRH="$ARCH" go build -ldflags "-buildid= -X main.GIT_VER=$VERinner -w -s" -trimpath "$FILEPATH"
    printf 'sha512: '
    sha512sum "$FILE$EXT"
    p=$(pwd -P)
    printf 'sig: '
    (
        cd $FILEPATH
        go run ../genSigs "$p/$FILE$EXT"
    )
    rm "$FILE$EXT"
    echo
}

sum ./cmd/example "$VER" windows amd64 example .exe
sum ./cmd/example "$VER" linux amd64 example
sum ./cmd/sign "$VER" windows amd64 sign .exe
sum ./cmd/sign "$VER" linux amd64 sign
